#!/usr/bin/env python3


import scipy as s
import ssfm.solver as solver
import ssfm.viewer as viewer


# Grid parameters
nz = 2**8
nt = 2**12

minz = 00.0
maxz = 10.0

mint = -160.0
maxt = +160.0

z = s.linspace(minz, maxz, nz)
t = s.linspace(mint, maxt, nt)


# Medium parameters
beta2 = -1.0
beta3 = +0.1
betas = [beta2, beta3]
gamma = +1.0


# Absorbing window
amp = 200.0
width = 0.01 * (maxt - mint)
absorber = amp / s.cosh((t - mint) / width) \
         + amp / s.cosh((t - maxt) / width)


# Initial condition
amp = 2.0
input = amp / s.cosh(t)


# Run solver
f, states, spectra = solver.nlse.integrate(z, t, input, betas, gamma, absorber)
minf = f.min()
maxf = f.max()


# Show intensity plots in dB and output spectrum.
# Time domain
viewer.timedomain(z, t, states,  db=True, dbmin=-40, cmap="gnuplot")
viewer.freqdomain(z, f, spectra, db=True, dbmin=-40, cmap="jet")
viewer.show()
