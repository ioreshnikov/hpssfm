import scipy as s
import matplotlib.pyplot as plot


def show():
    plot.show()


def viewer(y, x, z,
           db=False,
           dbmin=-50,
           cmap="gnuplot",
           xlabel="t",
           ylabel="z",
           skip=None,
           figure=True,
           colorbar="vertical"):
    if skip is not None:
        x = x[::skip]
        z = z[:, ::skip]
    im = abs(z)
    im = im / im.max()
    if db:
        im = 10 * s.log10(im)

    fig = None
    ax = None
    cb = None
    if figure:
        fig = plot.figure()
    plot.pcolormesh(x, y, im, cmap=cmap, rasterized=True)
    plot.xlim([min(x), max(x)])
    plot.ylim([min(y), max(y)])
    plot.xlabel(xlabel)
    plot.ylabel(ylabel)
    if colorbar is not None:
        pad = 0.20 if colorbar == "horizontal" else 0.05
        cb = plot.colorbar(orientation=colorbar, pad=pad)
    if db:
        plot.clim([dbmin, 0])
    return fig, ax, cb


def timedomain(z, t, states, cmap="afmhot", **kwargs):
    return viewer(z, t, states, cmap=cmap, **kwargs)


def freqdomain(z, f, spectra, cmap="rainbow", **kwargs):
    return viewer(z, f, spectra, cmap=cmap, xlabel="f", **kwargs)
